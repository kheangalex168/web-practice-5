import './App.css';
import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import MyCard from './components/Card'
import { Component } from "react";
import { Container, Row } from 'react-bootstrap';
import MyTable from './components/Table';

 class App extends Component {
    constructor(){
      super()
      this.state = {
        foods: [ 
        {
          id: 1,
          title: "Pizza",
          img: "https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/delish-homemade-pizza-horizontal-1542312378.png?crop=1.00xw:1.00xh;0,0&resize=480:*",
          qty: 0,
          price: 15,
          subtotal: 0,
          discount: 0,
          total: 0,
        },
        {
          id: 2,
          title: "Chicken",
          img: "https://i.ytimg.com/vi/-lwHfv0iMgY/maxresdefault.jpg",
          qty: 0,
          price: 5,
          subtotal: 0,
          discount: 0,
          total: 0,
        },
        {
          id: 3,
          title: "Burger",
          img: "https://assets.epicurious.com/photos/5c745a108918ee7ab68daf79/master/pass/Smashburger-recipe-120219.jpg",
          qty: 0,
          price: 2,
          subtotal: 0,
          discount: 0,
          total: 0,
        },
        {
          id: 4,
          title: "Coca",
          img: "https://bizweb.dktcdn.net/thumb/1024x1024/100/363/017/products/coca-cola.jpg?v=1566202790853",
          qty: 0,
          price: 3,
          subtotal: 0,
          discount: 0,
          total: 0,
        },
      ]
    }
  }

  OnAdd = (index) => {
    let tmp = [...this.state.foods]
    tmp[index].qty++
    tmp[index].subtotal = tmp[index].qty * tmp[index].price
      if(tmp[index].subtotal>0 && tmp[index].subtotal<20){
          tmp[index].discount = 5
          tmp[index].total = (tmp[index].subtotal * 0.95).toFixed(2)
      }
          else if(tmp[index].subtotal>=20 && tmp[index].subtotal<30){
            tmp[index].discount = 10
            tmp[index].total = (tmp[index].subtotal * 0.9).toFixed(2)
          }
          
          else if(tmp[index].subtotal>=30 && tmp[index].subtotal<40){
            tmp[index].discount = 20
            tmp[index].total = (tmp[index].subtotal * 0.8).toFixed(2)
          }
          
          else{
            tmp[index].discount = 30
            tmp[index].total = (tmp[index].subtotal * 0.7).toFixed(2)
          }
          
    this.setState({
      foods: tmp
    })
  }

  OnDelete = (index) => {
    let tmp = [...this.state.foods]
    tmp[index].qty--

    this.setState({
      foods: tmp
    })
  }

  OnClear = () => {
    let tmp = [...this.state.foods]
    tmp.map(item => {
      item.qty = 0
      item.total = 0
    })
    this.setState({ 
      foods: tmp
    })
  }

    render(){
  return (
    <Container>
      <Row>
   <MyCard foods={this.state.foods} OnAdd={this.OnAdd} OnDelete={this.OnDelete} />
   </Row>
   <Row>
   <MyTable foods={this.state.foods} OnClear={this.OnClear}/>
   </Row>
   </Container>
  );
    }
}

export default App
