import React from "react";
import { Component } from "react";
import { Card, Button, Col } from "react-bootstrap";

class MyCard extends Component {
  render() {
    return (
      <>
        {this.props.foods.map((item, index) => (
          <Col xs="3">
            <Card style={{ width: "18rem" }}>
              <Card.Img variant="top" src={item.img} />
              <Card.Body>
                <Card.Title>{item.title}</Card.Title>
                <Card.Text>price: {item.price} $</Card.Text>
                <Button variant="warning">{item.qty}</Button>{" "}
                <Button
                  variant="primary"
                  onClick={() => this.props.OnAdd(index)}
                >
                  Add
                </Button>{" "}
                <Button
                  disabled={item.qty === 0 ? true : false}
                  variant="danger"
                  onClick={() => this.props.OnDelete(index)}
                >
                  Delete
                </Button>
                <h2>Total: {item.total} $</h2>
              </Card.Body>
            </Card>
          </Col>
        ))}
      </>
    );
  }
}
export default MyCard;
