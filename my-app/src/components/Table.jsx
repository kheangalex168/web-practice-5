import React from "react";
import { Table, Button } from "react-bootstrap";

function MyTable({ foods, OnClear }) {
  let tmp = foods.filter((item) => {
    return item.qty > 0;
  });
  return (
    <>
      <Button variant="danger" onClick={OnClear}>
        Clear all
      </Button>
      <Button variant="warning">{tmp.length} count</Button>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>#</th>
            <th>name</th>
            <th>Quantity</th>
            <th>Price</th>
            <th>Total</th>
            <th>Discount</th>
            <th>Subtotal</th>
          </tr>
        </thead>
        <tbody>
          {tmp.map((item, i) => (
            <tr>
              <td>{i + 1}</td>
              <td>{item.title}</td>
              <td>{item.qty}</td>
              <td>${item.price}</td>
              <td>${item.subtotal}</td>
              <td>{item.discount}%</td>
              <td>${item.total}</td>
            </tr>
          ))}
        </tbody>
      </Table>
    </>
  );
}

export default MyTable;
